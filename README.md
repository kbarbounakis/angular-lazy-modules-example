#### Angular lazy modules example

This project is an example of usage of lazy modules in a angular cli project.

### Define lazy modules

One or more lazy modules may be defined in angular.json e.g.

        "architect": {
        "build": {
          "builder": "@angular-devkit/build-angular:browser",
          "options": {
            "outputPath": "dist/demo",
            "index": "src/index.html",
            "main": "src/main.ts",
            "polyfills": "src/polyfills.ts",
            "tsConfig": "src/tsconfig.app.json",
            "assets": [
              "src/favicon.ico",
              "src/assets"
            ],
            "lazyModules": [
              "src/app/test-plugin/test-plugin.module"
            ]
            ...
        
These modules are going to be compiled during serve and build

        + dist
           index.html
           ...
           runtime.js
           runtime.js.map
           ...
           src-app-test-plugin-test-plugin-module.js
           src-app-test-plugin-test-plugin-module.js.map
           ...

### Define runtime lazy modules

This project use environment.ts to define which are the modules that are going to be loaded during runtime.

        // environment.ts
        export const environment = {
          production: false,
          languages: ['el', 'en'],
          dynamicModules: [
            {
                "path": 'plugin',
                'loadChildren': 'src/app/test-plugin/test-plugin.module#TestPluginModule'
            }
          ]
        };

Important note: The path used to define a module must be always the same with path used in angular.json#lazyModules section.

### Load lazy module by using routing

Modify app.routing to load active lazy modules that are defined in environment.ts


    // app.routing.ts
    import {environment} from '../environments/environment';
    import {HelloComponent} from './hello.component';
    export const routes: Routes = [
        {
            "path": "",
            "component": HelloComponent 
        }
    ];
    routes.push.apply(routes, environment.dynamicModules);

