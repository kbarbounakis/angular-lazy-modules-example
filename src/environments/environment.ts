export const environment = {
  production: false,
  languages: ['el', 'en'],
  dynamicModules: [
    {
        "path": 'plugin',
        'loadChildren': 'src/app/test-plugin/test-plugin.module#TestPluginModule'
    }
  ]
};