import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TestPluginHomeComponent} from './components/test-plugin-home.component';
import {RouterModule, Routes} from '@angular/router';
import {TestPluginRouting} from './test-plugin.routing';

@NgModule({
    imports: [
        CommonModule,
        TestPluginRouting
    ],
    declarations: [
        TestPluginHomeComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
    providers: [
    ]
})
export class TestPluginModule {

    constructor() {
        //
    }
}
