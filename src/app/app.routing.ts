import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {environment} from '../environments/environment';
import {HelloComponent} from './hello.component';
export const routes: Routes = [
    {
        "path": "",
        "component": HelloComponent 
    }
];
routes.push.apply(routes, environment.dynamicModules);

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
