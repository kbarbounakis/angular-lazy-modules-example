import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-test-plugin-home',
    template: `
    <h1>{{title}}</h1>
    `,
    styleUrls: []
})
export class TestPluginHomeComponent implements OnInit {
    title = 'Hello from test plugin';
    constructor() { }

    ngOnInit() {

    }
}